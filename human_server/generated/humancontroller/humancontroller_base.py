# Generated by sila2.code_generator; sila2.__version__: 0.7.3
from __future__ import annotations

from abc import ABC, abstractmethod
from typing import TYPE_CHECKING

from sila2.framework.data_types.any import SilaAnyType
from sila2.server import FeatureImplementationBase, MetadataDict, ObservableCommandInstance

from .humancontroller_types import CustomCommand_Responses, MoveContainer_Responses, Site

if TYPE_CHECKING:
    from ...server import Server


class HumanControllerBase(FeatureImplementationBase, ABC):
    parent_server: Server

    def __init__(self, parent_server: Server):
        """

        This feature is designed to enable interaction between a human and an automated lab.
        This feature is supposed to be implemented by a server with a GUI running on some computer.
        When a command is issued on the server, the GUI should pop up a notification, that a new task appeared.
        The human can then fulfill the task accordingly, enter eventual responses and  mark it as finished.

        """
        super().__init__(parent_server=parent_server)

    @abstractmethod
    def MoveContainer(
        self,
        From: Site,
        To: Site,
        AdditionalWishes: str,
        *,
        metadata: MetadataDict,
        instance: ObservableCommandInstance,
    ) -> MoveContainer_Responses:
        """

        Moves a container from one location to another location



          :param From: old location

          :param To: new location

          :param AdditionalWishes:
            Additional wishes like for example preferred location, or removing the lid


          :param metadata: The SiLA Client Metadata attached to the call
          :param instance: The command instance, enabling sending status updates to subscribed clients

        """
        pass

    @abstractmethod
    def CustomCommand(
        self,
        Description: str,
        ResponseStructure: SilaAnyType,
        *,
        metadata: MetadataDict,
        instance: ObservableCommandInstance,
    ) -> CustomCommand_Responses:
        """

        Structure to issue any human understandable command.



          :param Description:
          Human readable description of the task. It should also contain information about the expected response
          that is not contained in its datatype description
          (in SiLA, the Any-datatype includes description fields in case of structures).


          :param ResponseStructure:
          The client has to specify the structure of the expected response in form of an Any-datatype.
          The response type will match this type.
          The parameter content is irrelevant but can for example contain suggestions.


          :param metadata: The SiLA Client Metadata attached to the call
          :param instance: The command instance, enabling sending status updates to subscribed clients

          :return:

              - Response: Response of the human


        """
        pass
