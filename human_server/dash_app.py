"""
A GUI similar to the main GUI for the sole purpose of demonstrating and observing the (GNN-)heuristic
"""
from __future__ import annotations

import time
import traceback

from typing import List, Dict
from dash import dcc, html, no_update, ALL
import plotly.graph_objects as go
import os
import dash
import logging
from dash.dependencies import State, Input, Output
from dash_extensions.enrich import MultiplexerTransform, DashProxy
from threading import Thread
from human_server.human_interface import HumanInterface, Command
from PIL import Image


class CommandTemplate(html.Div):
    def __init__(self, cmd: Command):
        answer_button = html.Button("Done", id={'type': 'aw_button', 'index': cmd.id})
        answer_field = dcc.Textarea(value=str(cmd.parameters), id={'type': 'aw_text', 'index': cmd.id})
        super(CommandTemplate, self).__init__(children=[html.Div(children=cmd.description),
                                                        answer_field,
                                                        answer_button,
                                                        ])


class HumanGui:
    human: HumanInterface
    app: DashProxy
    p: Thread
    running_commands: Dict[str, CommandTemplate]

    def __init__(self, human: HumanInterface, port=8054):
        self.human = human
        self.running_commands = dict()
        self.app = DashProxy("The SiLA-fied Human", prevent_initial_callbacks=True, transforms=[MultiplexerTransform()])
        self.p = Thread(target=self.app.run_server, daemon=True, kwargs=dict(debug=False, port=port), )
        #pic = go.Figure()
        #img_path = os.path.join(os.curdir, 'zookeeper.png')
        #pic.add_layout_image(source=Image.open(img_path))
        self.app.layout = html.Div(children=[
            html.H1(children='''A web application for a human to work for a lab automation framework'''),
            html.Div(children='List of active given commands'),
            html.Div(id='commands', children=[
            ], style={'vertical-align': 'top', 'display': 'inline-block'}),
            dcc.Interval(
                id='interval-component',
                interval=200,
                n_intervals=0
            ),
            dcc.Graph(figure=self.create_picture()),
        ])

        @self.app.callback(
            Output(component_id='commands', component_property='children'),
            Input(component_id='interval-component', component_property='n_intervals'),
        )
        def update_command_list(n_intervals):
            cmd_list = [html.Div(children="="*200)]
            update = False
            for command in self.human.running_commands:
                if command.id not in self.running_commands:
                    self.running_commands[command.id] = CommandTemplate(command)
                    update = True
            if not update:
                return no_update
            for template in self.running_commands.values():
                cmd_list.append(template)
                cmd_list.append(html.Div(children="-"*200))
            return cmd_list

        @self.app.callback(
            Input(component_id={'type': 'aw_button', 'index': ALL}, component_property='n_clicks'),
            State(component_id={'type': 'aw_text', 'index': ALL}, component_property='value'),
            State(component_id={'type': 'aw_text', 'index': ALL}, component_property='id'),
            Output(component_id='commands', component_property='children'),
        )
        def give_answer(n_clicks, value, ids_txt):
            trigger = dash.callback_context.triggered[0]
            cmd_id = trigger['prop_id'].split('"')[3]
            if trigger['value']:
                for id_txt, value in zip(ids_txt, value):
                    if id_txt['index'] == cmd_id:
                        if self.human.answer_command(answer=value, cmd_id=cmd_id):
                            self.running_commands = {}
            return no_update

    def run(self):
        if self.p.is_alive():
            logging.warning('Server is already running. Restarting server')
            self.stop()
        logging.getLogger('werkzeug').setLevel(logging.ERROR)
        self.p.start()

    def stop(self):
        print("Sorry, I don't know, how to stop")

    def create_picture(self):
        # Create figure
        fig = go.Figure()

        # Constants
        img_width = 1000
        img_height = 900
        scale_factor = 0.5

        # Add invisible scatter trace.
        # This trace is added to help the autoresize logic work.
        fig.add_trace(
            go.Scatter(
                x=[0, img_width * scale_factor],
                y=[0, img_height * scale_factor],
                mode="markers",
            )
        )

        # Configure axes
        fig.update_xaxes(
            visible=False,
            range=[0, img_width * scale_factor]
        )

        fig.update_yaxes(
            visible=False,
            range=[0, img_height * scale_factor],
            # the scaleanchor attribute ensures that the aspect ratio stays constant
            scaleanchor="x"
        )

        # Add image
        fig.add_layout_image(
            dict(
                x=0,
                sizex=img_width * scale_factor,
                y=img_height * scale_factor,
                sizey=img_height * scale_factor,
                xref="x",
                yref="y",
                opacity=1.0,
                layer="below",
                sizing="stretch",
                source=Image.open('whip.png'))
        )

        # Configure other layout
        fig.update_layout(
            width=img_width * scale_factor,
            height=img_height * scale_factor,
            margin={"l": 0, "r": 0, "t": 0, "b": 0},
        )
        return fig
