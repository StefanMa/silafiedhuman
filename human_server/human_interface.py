from typing import NamedTuple, List, Any, Optional, Dict
from dataclasses import dataclass
import uuid
import logging
import json


def str_to_list(s: str):
    return s.strip()


def parse(s: str):
    for fct in [int, float, json.loads]:
        try:
            return fct(s)
        except:
            pass
    return s


@dataclass
class Command:
    description: str
    response_type: str
    id: str
    parameters: Optional[Any] = None
    answer: Optional[Any] = None

    def try_answer(self, answer: Any) -> bool:
        self.answer = parse(answer)
        return True


class HumanInterface:
    _running_commands: Dict[str, Command]
    _answered_commands: Dict[str, Command]

    def __init__(self):
        self._running_commands = dict()
        self._answered_commands = dict()

    def give_command(self, description: str, response_type: str, parameter: Any = None) -> str:
        cmd_id = str(uuid.uuid1())
        self._running_commands[cmd_id] = (Command(description, response_type, cmd_id, parameters=parameter))
        return cmd_id

    def answer_command(self, cmd_id: str, answer=None) -> bool:
        if cmd_id not in self._running_commands:
            logging.warning(f"No command with uuid {cmd_id} is running")
        # test validity of the answer
        cmd = self._running_commands[cmd_id]
        if cmd.try_answer(answer):
            self._answered_commands[cmd_id] = cmd
            self._running_commands.pop(cmd_id)
            return True
        return False

    def command_finished(self, cmd_id: str) -> bool:
        return cmd_id in self._answered_commands

    def command_by_uuid(self, cmd_id: str) -> Optional[Command]:
        if cmd_id in self._running_commands:
            return self._running_commands[cmd_id]
        if cmd_id in self._answered_commands:
            return self._answered_commands[cmd_id]
        return None

    @property
    def running_commands(self) -> List[Command]:
        return list(self._running_commands.values())
