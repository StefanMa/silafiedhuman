# SilafiedHuman

This Project is a realization of an idea how a human can receive
and execute/answer commands behind a SiLA interface.
The project a SiLA server that takes the commands and a dash-app that
presents the commands and lets the human answer and mark them as done.

## Installation
Installation with the usual

*pip install [-e] .*

## Demo
1. Go into the root directory and start the demo_script in the console:
*python demo_script.py*
. This starts a sila-server and a dash app. 

2. Go to http://localhost:8054/ in your browser to see the dash-interface
3. Press enter two times to issue sample commands to the sila-server
4. Observe the received commands in the dash-app
5. Try answering

## Project status
Played around while sitting tired in a train ;-)