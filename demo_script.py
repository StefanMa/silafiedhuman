"""
A script to demonstrate the functionality of the human behind a SiLA interface
"""
from human_server import Server
from human_server import Client
from sila2.framework.data_types.any import SilaAnyType
from threading import Thread
import time

server = Server()
server.start_insecure(address='127.0.0.1', port=50082)

client = Client("127.0.0.1", 50082, insecure=True)

input("Press enter to issue move command\n")
cmd1 = client.HumanController.MoveContainer(From=('Pipetting Robot', 0),
                                            To=("Incubator", 6),
                                            AdditionalWishes="Be careful!")

input("Press enter to issue custom commands\n")
cmd2 = client.HumanController.CustomCommand(
    Description="What is nine times six?",
    ResponseStructure=SilaAnyType(
        type_xml="<DataType><Basic>Real</Basic></DataType>",
        value=54
    )
)


def check_answer():
    while not cmd2.done:
        time.sleep(.1)
    answer = cmd2.get_responses().Response.value
    if answer == 42:
        print("What a clever human!")
    else:
        print("Wrong!")


Thread(target=check_answer, daemon=True).start()


cmd3 = client.HumanController.CustomCommand(
    Description="List all empty slots in the incubator(starting with 0)!",
    ResponseStructure=SilaAnyType(
        type_xml="<DataType><List><DataType><Basic>Integer</Basic></DataType></List></DataType>",
        value=[2, 3, 4, 7]
    )
)

input("Press enter to stop server\n")
print("Because of problems with stopping servers, you probably have to kill the script manually with (several) Ctrl+C")
server.stop()
print('server stopped')
